package pl.fissst.bet.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import pl.fissst.bet.dto.Match;
import pl.fissst.bet.dto.mock.MockMatchUtil;


@Named
@SessionScoped
public class MatchDAO implements BaseDAO<Match>, Serializable {

	
	private static final long serialVersionUID = 2634837676193644563L;
	private  List<Match> matches;

	 MockMatchUtil mockMatchUtil;
	
	public List<Match> getMatches() {
		return matches;
	}

	public void setMatches(List<Match> matches) {
		this.matches = matches;
	}

	public List<Match> addMatch() {
		 
		  matches.add(mockMatchUtil.generate());
	
		  return  matches;
	}
	
	

/*	@Override
	public Match findAll() {
		// TODO Auto-generated method stub
		return null;
	}*/

	@Override
	public Match findById(Long id) {
		for(Match m:matches) {
			if(m.getId().compareTo(id)==Long.valueOf("0")) {
				return m;
			}
		}
		return null;
	}


	public Match add(Match entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Match update(Match entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Match entity) {
		matches.remove(entity);
		
	}


	@Override
	public List<Match> findAll() {

		 if(matches==null) {
		
			 Match match;
			  List<Match> matches2= new ArrayList<Match>();
			  for(int i =0;i<10;i++) {
			match = mockMatchUtil.generate();
			matches2.add(match);
			  }
			matches=matches2;
			 // matches.add(MockMatchUtil.generate());
			
			 }
			  return  matches;
	}
}


