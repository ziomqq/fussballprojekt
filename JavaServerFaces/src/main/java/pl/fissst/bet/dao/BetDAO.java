package pl.fissst.bet.dao;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import pl.fissst.bet.dto.Bet;
import pl.fissst.bet.dto.mock.MockBetUtil;;

@Named
@SessionScoped
public class BetDAO implements BaseDAO<Bet>, Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1131167234102502505L;
	private List<Bet> bets;
	
	@PostConstruct
	private void initMock() {
		bets = new MockBetUtil().generateList();
	}
	
	public List<Bet> getBetsByUserID(Long UserID){
		return bets;
	}
	
	public void setBetByUserID(Bet bet, Long UserID) {
		//TODO
	}

	@Override
	public List<Bet> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Bet findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Bet add(Bet entity) {
		if(entity == null) {
			return null;
		}
		bets.add(entity);
		return entity;
	}

	@Override
	public Bet update(Bet entity) {
		if (entity == null) return null;
		
		for(int i=0; i < bets.size(); i++ ) {
			if (bets.get(i).getId() == entity.getId()) {
				bets.set(i, entity);
				return entity;
				
			}
		}
		
		return null;
	}

	@Override
	public void delete(Bet entity) {
		if (entity != null) {
			bets.remove(entity);
			
		}
		
	}

}
