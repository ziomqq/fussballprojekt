package pl.fissst.bet.dao;

import java.util.List;

public interface BaseDAO<E>  {

	List<E> findAll();
	E findById(Long id);
	E add(E entity);
	E update(E entity);
	void delete(E entity);
}
