package pl.fissst.bet.dao;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import pl.fissst.bet.dto.User;
import pl.fissst.bet.dto.mock.MockUserUtil;

@Named
@SessionScoped
public class UserDAO implements BaseDAO<User>, Serializable{

	private static final long serialVersionUID = -8671587664662560201L;
	private List<User> users;

	public void setUsers(List<User> users) {
		this.users = users;
	}
	@Override
	public List<User> findAll() {
		if (users == null) {
			users = new MockUserUtil().generateList();
		}
		return users;
	}
	@Override
	public User findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}
		@Override
	public User add(User entity) {
		if (entity == null) {
			return null;	
		}
		 users.add(entity);
		 return entity;
	}
	@Override
	public User update(User entity) {
		if (entity == null) {
			return null;	
		}
		 entity.setVersion(entity.getVersion() + 1L);
		 return entity;
	}
	@Override
	public void delete(User entity) {
		users.remove(entity);
	}
	public List<User> getUsers() {
		return users;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}