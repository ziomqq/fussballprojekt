package pl.fissst.bet.service;

import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import pl.fissst.bet.dao.UserDAO;
import pl.fissst.bet.dto.User;

@ManagedBean
@Named
@SessionScoped
public class UserService implements Serializable {
	
	private static final long serialVersionUID = 2634833386193640073L;
	@Inject
	private UserDAO userDAO;
	
	public List<User> findAll() {
		return userDAO.findAll();
	}
	public User insert(User user) {
		return userDAO.add(user);
	}
	public void delete(User entity) {
		userDAO.delete(entity);
	}
	public List<User> getMatchesDAO() {
		return userDAO.getUsers();
	}
}