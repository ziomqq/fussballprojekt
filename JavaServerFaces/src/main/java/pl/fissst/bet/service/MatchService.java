package pl.fissst.bet.service;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

import pl.fissst.bet.dao.MatchDAO;
import pl.fissst.bet.dto.Match;

@ManagedBean
@Named
@SessionScoped
public class MatchService implements  Serializable{
	
	
	
	
	private static final long serialVersionUID = 2634833386193644563L;
	private MatchDAO matchDAO = new MatchDAO();
	
	
	
	 public List<Match> createMatches() {
	        return matchDAO.findAll();
	    }
	
	public Match getByID(Long id){
		return matchDAO.findById(id);
	}
	
	public void delete(Match entity) {
		matchDAO.delete(entity);
		
	}
	
	public List<Match> getMatchesDAO() {
		return matchDAO.getMatches();
	}
	
	

}
