package pl.fissst.bet.service;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import pl.fissst.bet.dao.BetDAO;
import pl.fissst.bet.dto.Bet;

@SessionScoped
@Named
@ManagedBean
public class BetService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4326206703443962258L;
	@Inject
	private BetDAO betDAO;
	
	public List<Bet> getBetsByUserID(Long userId) {
		return betDAO.getBetsByUserID(userId);
	}
	
	public Bet insertBet(Bet bet) {
		return betDAO.add(bet);
	}
	
	public Bet editBet(Bet bet) {
		return betDAO.update(bet);
	}
	
	public void delBet(Bet bet) {
		betDAO.delete(bet);
	}
	
}
