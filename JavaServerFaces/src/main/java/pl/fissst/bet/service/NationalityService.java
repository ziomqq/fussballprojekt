package pl.fissst.bet.service;

import pl.fissst.bet.dto.Nationality;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ManagedBean(name = "nationalityService")
@ApplicationScoped
public class NationalityService implements Serializable {

	private List<Nationality> nationalities;

	@PostConstruct
	public void init() {
		nationalities = mockNationalities();
	}

	public void addNationality(Nationality nationality) {
		nationality.setId(findId());
		nationalities.add(nationality);
	}

	private long findId() {
		long i;
		for (i=0; i<Long.MAX_VALUE; i++) {
			final Long id = i;
			Optional<Nationality> any = nationalities
					.stream()
					.filter(nat -> id.equals(nat.getId()))
					.findAny();
			if (!any.isPresent()) {
				break;
			}
		}
		return i;
	}

	public void deleteNationality(Nationality nationality) {
		nationalities.remove(nationality);
	}

	public List<Nationality> getNationalities() {
		return nationalities;
	}

	public List<Nationality> mockNationalities() {
		nationalities = new ArrayList<>();

		nationalities.add(new Nationality(0L, "Polish", "PL"));
		nationalities.add(new Nationality(1L, "German", "DE"));
		nationalities.add(new Nationality(2L, "French", "FR"));

		return nationalities;
	}
}