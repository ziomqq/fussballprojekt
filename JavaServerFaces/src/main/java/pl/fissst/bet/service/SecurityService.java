package pl.fissst.bet.service;

import java.io.Serializable;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.enterprise.context.SessionScoped;

import pl.fissst.bet.dto.User;
import pl.fissst.bet.dto.mock.MockUserUtil;

@SessionScoped
@ManagedBean
public class SecurityService implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8563952721020861367L;
	private List<User> users;
	
	public User getUser() {
		users.add(new MockUserUtil().generate());
		return users.get(0);
	}

}
