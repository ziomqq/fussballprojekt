package pl.fissst.bet.view;

import pl.fissst.bet.dto.Nationality;
import pl.fissst.bet.service.NationalityService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

@ManagedBean
@ViewScoped
public class NationalityManagerView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L+56;

	@ManagedProperty("#{nationalityService}")
	private NationalityService nationalityService;

	private String name;

	private String countryCode;

	private String result;

	private List<Nationality> nationalities;

	@PostConstruct
	public void init() {
		nationalities = nationalityService.getNationalities();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getResult() {
		return result;
	}

	public List<Nationality> getNationalities() {
		return nationalities;
	}

	public void addNationality() {
		Nationality nationality = new Nationality();

		nationality.setName(name);
		nationality.setCountryCode(countryCode);

		nationalityService.addNationality(nationality);

		result = "Nationality added successfully. Count: " + nationalityService.getNationalities().size();
	}

	public void deleteNationality(Nationality nationality) {
		nationalityService.deleteNationality(nationality);
	}

	public void setNationalityService(NationalityService nationalityService) {
		this.nationalityService = nationalityService;
	}
}
