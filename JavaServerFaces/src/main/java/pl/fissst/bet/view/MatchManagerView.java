package pl.fissst.bet.view;


import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
// import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import pl.fissst.bet.dto.Match;
import pl.fissst.bet.service.MatchService;


@Named
@SessionScoped
@ManagedBean
public class MatchManagerView implements Serializable{
	
	private static final long serialVersionUID = 7090877540444288998L;
	
	
	private List<Match> matches;
	private Match selectedMatch;
	
	
	
	
	public Match getSelectedMatch() {
		return selectedMatch;
	}

	public void setSelectedMatch(Match selectedMatch) {
		this.selectedMatch = selectedMatch;
	}

	public List<Match> getMatches() {
		return matches;
	}

	public void setMatches(List<Match> matches) {
		this.matches = matches;
	}

	@Inject
    private MatchService service;
 
    @PostConstruct
    public void init() {
    	matches = service.createMatches();
    }
     
    public void deleteMatch() {
    	service.delete(selectedMatch);
    	//setMatches(service.getMatchesDAO());
    	getMatches().remove(selectedMatch);
    	
    }
   
   
    

}
