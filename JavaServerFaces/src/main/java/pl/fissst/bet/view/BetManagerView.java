package pl.fissst.bet.view;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

import pl.fissst.bet.dto.Bet;
import pl.fissst.bet.dto.Match;
import pl.fissst.bet.service.BetService;
import pl.fissst.bet.service.MatchService;

@Named
@SessionScoped
@ManagedBean
public class BetManagerView implements Serializable {

	private static final long serialVersionUID = 1L + 1;

	@Inject
	private BetService betService;
	
	@Inject
	@ManagedProperty("#{matchService}")
	private MatchService matchService;
	
	private List<Bet> betsList;
	private List<Match> matchesList;
	private Match match;
	private Bet newBet;

	public Bet getNewBet() {
		return newBet;
	}
	public void setNewBet(Bet newBet) {
		this.newBet = newBet;
	}
	public Match getMatch() {
		return match;
	}
	public void setMatch(Match match) {
		this.match = match;
	}
	
	public List<Match> getMatchesList() {
		return matchesList;
	}
	public void setMatchesList(List<Match> matchesList) {

		this.matchesList = matchesList;
	}

	private Bet selectedBet = null;
	public Bet getSelectedBet() {
		return selectedBet;
	}
	public void setSelectedBet(Bet selectedBet) {
		this.selectedBet = selectedBet;
	}
	
	
	public List<Bet> getBetsList(){
		return betsList;
	}
	public void setBetsList(List<Bet> betsList){
		this.betsList = betsList;
	}
	
	@PostConstruct
    public void init() {
		betsList = betService.getBetsByUserID(Long.valueOf(1));
		matchesList = matchService.createMatches();
    }
	public void createNewBet() {
		newBet = new Bet();
	}
	
	public void buttonAdd() {
		Calendar cal = Calendar.getInstance();
		Random rand = new Random();
		
		newBet.setDate(cal.getTime());
		newBet.setId(Long.valueOf(rand.nextInt(100 - 1 + 1) + 1));
		newBet.setMatch(match);
		betService.insertBet(newBet);
    }
	
	public void buttonEdit() {
		Calendar cal = Calendar.getInstance();
		selectedBet.setDate(cal.getTime());
		betService.editBet(selectedBet);
	}

	public void buttonActionEdit(ActionEvent actionEvent) {
		if(selectedBet == null) {
	        FacesMessage msg = new FacesMessage("You must select bet to edit!");
	        FacesContext.getCurrentInstance().addMessage(null, msg);
		}
		else {
			RequestContext.getCurrentInstance().execute("PF('betEditDlg').show()");
		}
    }

	public void onRowSelect(SelectEvent event ) {
		setSelectedBet((Bet)event.getObject());
	}
	
	public void deleteBet() {
		if(selectedBet == null) {
	        FacesMessage msg = new FacesMessage("You must select bet to delete!");
	        FacesContext.getCurrentInstance().addMessage(null, msg);
		}
		else {
			RequestContext.getCurrentInstance().execute("PF('betDeleteDlg').show()");
		}
	}
	public void btnDeleteBet() {
		betService.delBet(selectedBet);
		selectedBet = null;
	}
	
	

	
}


