package pl.fissst.bet.view;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.context.RequestContext;
import pl.fissst.bet.dto.User;
import pl.fissst.bet.service.UserService;

@Named
@SessionScoped
@ManagedBean
public class UserManagerView implements Serializable {

	private static final long serialVersionUID = 1L + 1;
	@Inject
	private UserService userService;
	private List<User> userList;
	private User userDetail;
	private User selectedUser = null;
	
	public List<User> getUserList() {
		return userList;
	}
	public void setUserList(List<User> userList) {
		this.userList = userList;
	}
	public User getUserDetail() {
		return userDetail;
	}
	public void setUserDetail(User userDetail) {
		this.userDetail = userDetail;
	}
	public void onAddNewUser() {
		userDetail = new User();
	}
	public void onAddNewUserDialog() {
	  	userService.insert(userDetail);
	  	userDetail = null;
	  	FacesMessage msg = new FacesMessage("New User added");
	    FacesContext.getCurrentInstance().addMessage(null, msg);
	}  
	public User getSelectedUser() {
		return selectedUser;
	}
	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}
	public void onActionEditUser(ActionEvent actionEvent) {
		if(selectedUser == null) {
		    FacesMessage msg = new FacesMessage("You must select user to edit!");
		    FacesContext.getCurrentInstance().addMessage(null, msg);
		}
		else {
			RequestContext.getCurrentInstance().execute("PF('dlgEditUser').show()");
		}
	}
	public void onEditUserDialog() {
		FacesMessage msg = new FacesMessage("User editted");
	    FacesContext.getCurrentInstance().addMessage(null, msg);
	}
	public void onDeleteUser() {
		userService.delete(selectedUser);
		FacesMessage msg = new FacesMessage("User deleted");
	    FacesContext.getCurrentInstance().addMessage(null, msg);
    }
	@PostConstruct
	public void init() {
	    userList = userService.findAll();
	}	
}