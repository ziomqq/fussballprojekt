package pl.fissst.bet.view;

import javax.enterprise.inject.spi.CDI;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import pl.fissst.bet.dto.Match;
import pl.fissst.bet.service.MatchService;

@FacesConverter("matchConverter")
public class MatchConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		if(arg2 != null && arg2.trim().length() > 0) {
            try {
                MatchService service = CDI.current().select(MatchService.class).get();
                return service.getByID(Long.valueOf(arg2));
            } catch(NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid theme."));
            }
        }
        else {
            return null;
        }
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if(arg2 == null) {
			return null;
        }

		if (!(arg2 instanceof Match)) {
			return null;
		}

		Match match = (Match) arg2;
		
		Long id = match.getId();
		if (id  == null) {
			return null;
		}
		
		return id.toString();
	}

}
