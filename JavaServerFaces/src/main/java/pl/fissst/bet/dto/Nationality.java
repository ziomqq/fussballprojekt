package pl.fissst.bet.dto;

public class Nationality {

  private Long id;
  
  private String name;
  
  private String countryCode;

  public Nationality() {}

  public Nationality(Long id, String name, String countryCode) {
  	this.id = id;
  	this.name = name;
  	this.countryCode = countryCode;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }
  
}
