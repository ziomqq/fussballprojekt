package pl.fissst.bet.dto;

import pl.fissst.bet.dto.mock.MockCompetitionUtil;

public class Competition {

  private Long id;
  private String name;
  private MockCompetitionUtil mockData;
  
  
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
  
  
  @Override
	public String toString() {
	  return name;
	}
  
  
  
  
  
  
  
  
  
}
