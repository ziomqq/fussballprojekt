package pl.fissst.bet.dto.mock;

import pl.fissst.bet.dto.Nationality;
import pl.fissst.bet.dto.User;

public class MockNationalityUtil extends MockUtil<Nationality>{
	
	@Override
	public Nationality generate()
	{
		Nationality nationality = new Nationality();
		long generatedLong = 1l + (long) (Math.random() * (10000l - 1l));
			nationality.setId(generatedLong);
			nationality.setCountryCode(generateSomeString());
			nationality.setName(generateSomeString());
		
		return nationality;
	}
	
	
}
