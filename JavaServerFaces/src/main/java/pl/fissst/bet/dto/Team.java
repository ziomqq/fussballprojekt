package pl.fissst.bet.dto;

public class Team {

  private Long id;
  
  private Nationality nationality;
  
  private String name;
  
  private String shortcut;
  
  public Long getId() {
    return id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public Nationality getNationality() {
    return nationality;
  }
  
  public void setNationality(Nationality nationality) {
    this.nationality = nationality;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getShortcut() {
    return shortcut;
  }

  public void setShortcut(String shortcut) {
    this.shortcut = shortcut;
  }
  
  @Override
	public String toString() {
	  return name;
	}
  
}
