package pl.fissst.bet.dto.mock;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;

public abstract class MockUtil<T> {

	public List<T> generateList() {
		List<T> list = new ArrayList<T>();
		
		//Random random = new Random();
		int range = 12;
		for(int i=1;i<=range;i++){
			list.add(generate());
		}
		return list;
	}
	public abstract T generate();
	
	protected String generateSomeString() {
		int length = 5;
	    boolean useLetters = true;
	    boolean useNumbers = false;
	    String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
	    String upperGeneratedString = generatedString.toUpperCase();
	    return upperGeneratedString;
	}
}
