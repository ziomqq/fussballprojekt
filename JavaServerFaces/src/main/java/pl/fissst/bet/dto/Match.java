package pl.fissst.bet.dto;

import java.util.Date;

public class Match {

  private Long id;
  
  private Competition competition;
  
  private String description;
  
  private Team team1;
  
  private Team team2;

  private Integer goal1;

  private Integer goal2;
  
  private Date date;

  
  
  Competition generateCompetition = new Competition();
  
  
  
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Competition getCompetition() {
    return competition;
  }

  public void setCompetition(Competition competition) {
    this.competition = competition;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Team getTeam1() {
    return team1;
  }

  public void setTeam1(Team team1) {
    this.team1 = team1;
  }

  public Team getTeam2() {
    return team2;
  }

  public void setTeam2(Team team2) {
    this.team2 = team2;
  }

  public Integer getGoal1() {
    return goal1;
  }

  public void setGoal1(Integer goal1) {
    this.goal1 = goal1;
  }

  public Integer getGoal2() {
    return goal2;
  }

  public void setGoal2(Integer goal2) {
    this.goal2 = goal2;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  @Override
	public String toString() {
	  StringBuilder string = new StringBuilder();
	  if (competition != null) {
		  string.append(competition);
		  string.append(" ");
	  }
	  
	  if (team1 != null) {
		  string.append(team1);
	  }
	  
	  string.append(":");
	  if (team2 != null) {
		  string.append(team2);
	  }
	  
      return string.toString();
	}
  
}
