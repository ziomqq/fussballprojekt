package pl.fissst.bet.dto;

public class User {
	
  private Long id;
  private String name;
  private String password;
  private boolean permission;
  private Long credits;
  private Long version = 0L;

  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public String getPassword() {
    return password;
  }
  public void setPassword(String passoword) {
    this.password = passoword;
  }
  public boolean isPermission() {
	return permission;
  }
  public void setPermission(boolean permission) {
	this.permission = permission;
  }
  public Long getCredits() {
	return credits;
  }
  public void setCredits(Long credits) {
	this.credits = credits;
  }
  public Long getVersion() {
  	return version;
  }
  public void setVersion(Long version) {
  	this.version = version;
  }
}
