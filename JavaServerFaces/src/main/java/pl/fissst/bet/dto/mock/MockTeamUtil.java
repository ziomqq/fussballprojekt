package pl.fissst.bet.dto.mock;

import pl.fissst.bet.dto.Team;
import pl.fissst.bet.dto.User;
import pl.fissst.bet.dto.Nationality;

public class MockTeamUtil extends MockUtil<Team> {
	
	@Override
	public  Team generate()
	{
		Team team = new Team();
		long generatedLong = 1l + (long) (Math.random() * (10000l - 1l));
			team.setId(generatedLong);
			team.setNationality(new MockNationalityUtil().generate());
			team.setName(generateSomeString());
			team.setShortcut("ExampleShourtcut:");
			
		
		return team;
	}
}
