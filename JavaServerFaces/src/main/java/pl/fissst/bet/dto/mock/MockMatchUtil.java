package pl.fissst.bet.dto.mock;

import java.util.Date;
import java.util.Random;

import pl.fissst.bet.dto.Match;
import pl.fissst.bet.dto.User;

public class MockMatchUtil  {

	
	
public static Match generate() {
		
			Match mockDataMatch = new Match();
			MockCompetitionUtil generateCompetition = new MockCompetitionUtil();
			MockTeamUtil generateTeam = new MockTeamUtil();
			
			Long id;
			Integer goal1;
			Integer goal2;  
			Date date;
			
			//Generating random
			Random rand = new Random();
			Integer randomNumber,randomNumber1,randomNumber2;
			randomNumber  = rand.nextInt(50);
			randomNumber1 = rand.nextInt(5);
			randomNumber2 = rand.nextInt(5);
			goal1 = randomNumber1;
			goal2 = randomNumber2;
			
			
			/* Making the right date
			 * 1530000000 = 26.06.2018, 10:00:00
			 * 1530100000 = 27.06.2018, 13:46:40
			 * and so on...
			 * converter at website check the following link ... = "http://timestamp.online/"	
			 */
			int i = rand.nextInt(10);
			long addOneDay = 100000;
			long unixSeconds = 1530000000+addOneDay*i;
			date = new java.util.Date(unixSeconds*1000L); 
			
			//set Id
			id = generateCompetition.generate().getId();
			mockDataMatch.setId(id);
			//set Competition
			mockDataMatch.setCompetition(generateCompetition.generate());
			//set Description
			String description = "Description from match :"+id;
			mockDataMatch.setDescription(description);
			//set Team1
			mockDataMatch.setTeam1(generateTeam.generate());
			//set Team2
			mockDataMatch.setTeam2(generateTeam.generate());
			//set Goal1 
			mockDataMatch.setGoal1(goal1);
			//set Goal2 
			mockDataMatch.setGoal2(goal2);
			//set Date
			mockDataMatch.setDate(date);
			
		return mockDataMatch;
	}
}
