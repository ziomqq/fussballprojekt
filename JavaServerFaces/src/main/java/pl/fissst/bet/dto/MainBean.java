package pl.fissst.bet.dto;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

import org.primefaces.component.tabview.TabView;
import org.primefaces.event.TabChangeEvent;

import java.io.Serializable;

@ManagedBean
@ApplicationScoped
@Named
public class MainBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int activeIndexTab = 0;
	
	public int getActiveIndexTab() {
		return activeIndexTab;
	}
	

	public void setActiveIndexTab(int activeIndex) {
		this.activeIndexTab = activeIndex;
	}
	

	public void onTabChange(TabChangeEvent event) {
		 setActiveIndexTab(((TabView) event.getSource()).getActiveIndex());
		    }
	

}