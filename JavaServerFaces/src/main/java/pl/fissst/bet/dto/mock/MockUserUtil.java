package pl.fissst.bet.dto.mock;


import org.apache.commons.lang.RandomStringUtils;

import pl.fissst.bet.dto.User;

public class MockUserUtil extends MockUtil<User> {
	
	@Override
	public User generate()
	{
		User user = new User();	
		user.setId(userIdGenerate());
		user.setName(generateSomeString());
		user.setPassword(passwordGenerate());
		user.setPermission(userPermission());
		user.setCredits(userCreditsGenerate());
		return user;
	}
	private String passwordGenerate() {
		int length = 10;
		boolean useLetters = true;
		boolean useNumbers = true;
		String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
		return generatedString;
	}
	private long userIdGenerate() {
		int length = 3;
		boolean useLetters = false;
		boolean useNumbers = true;
		String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
		long userIdGenerate=Long.valueOf(generatedString).longValue();
		return userIdGenerate;
	}
	private boolean userPermission() {
		return true ;
	}
	private long userCreditsGenerate() {
		return 1000;
	}
}
	

