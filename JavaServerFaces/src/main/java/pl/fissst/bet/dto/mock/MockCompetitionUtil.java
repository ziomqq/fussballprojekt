package pl.fissst.bet.dto.mock;

import java.util.Random;
import pl.fissst.bet.dto.Competition;

public class MockCompetitionUtil {

	public Competition generate() {
		
		Competition mockData = new Competition();
		
			Integer randomNumber;
			Random rand = new Random();
			randomNumber = rand.nextInt(50)+1;
			long id = randomNumber;
			String name = "user"+randomNumber;
			mockData.setId(id);
			mockData.setName(name);
			
		return mockData;
	}
}
