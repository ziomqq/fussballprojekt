package pl.fissst.bet.dto;

import java.util.Date;

public class Bet {

  private Long id;
  
  private User user;
  
  private Match match;
  
  private Integer goal1;
  
  private Integer goal2;
  
  private Date date;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }
 
  public void setUser(User user) {
    this.user = user;
  }

  public Match getMatch() {
    return match;
  }

  public void setMatch(Match match) {
    this.match = match;
  }

  public Integer getGoal1() {
    return goal1;
  }

  public void setGoal1(Integer goal1) {
    this.goal1 = goal1;
  }

  public Integer getGoal2() {
    return goal2;
  }

  public void setGoal2(Integer goal2) {
    this.goal2 = goal2;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }
  
}
