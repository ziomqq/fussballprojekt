package pl.fissst.bet.dto.mock;

import java.util.Date;
import java.util.Random;

import pl.fissst.bet.dto.Bet;

public class MockBetUtil extends MockUtil<Bet> {

	@Override
	public Bet generate() {

		Bet bet = new Bet();
		Random rand = new Random();
		
		bet.setId(Long.valueOf(rand.nextInt(100 - 1 + 1) + 1));
		bet.setGoal1(rand.nextInt(10 - 0 + 1) + 0);
		bet.setGoal2(rand.nextInt(10 - 0 + 1) + 0);
		bet.setDate(new Date());
		bet.setMatch(MockMatchUtil.generate());
		bet.setUser(new MockUserUtil().generate());
		
		return bet;
	}

}
